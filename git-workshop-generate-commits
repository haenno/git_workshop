#!/bin/sh
# This plumbing command generates and commits some random changes.


# ---  S U B R O U T I N E S  --- #
print_help_message() {
    echo "usage: `basename $0` [-b <branch>] [-f <name>] [-n <number>] [-o <dir>] [-p <parent>]"
    echo
    echo "This script generates and commits some random changes."
    echo
    echo "options:"
    echo -e "  -b\tcommit to and if necessary create <branch>"
    echo -e "  -f\twrite to <file>"
    echo -e "  -n\tgenerate only <number> commits (default: infinity)"
    echo -e "  -o\twrite files to <dir> (default: \"$output_directory\")"
    echo -e "  -p\tfork the branch specified with -b from <parent>"
}

generate_commit() {
    # clear flags
    unset changed

    # create files directory if it is not yet present
    mkdir -p $output_directory

    # generate random content
    content=`head -n1 /dev/urandom | base64 | tr "[:upper:]0123456789+/=" "[:lower:]abcdefghijklm"`

    # determine output file
    if [ -n "$output_file" ]; then
        file_name=$output_file
    else
        file_name=`expr substr "$content" 1 2`
    fi
    if [ -f $output_directory/$file_name ]; then
        changed=1
    fi

    # write and massage file
    temp_file=`mktemp`
    out_file=$output_directory/$file_name
    if [ -f $out_file ]; then
        cat $out_file > $temp_file
    fi
    echo $content >> $temp_file
    random_number=`shuf -i 0-9 -n 1`
    if [ $random_number -ge 5 ]; then
        sed -i 's/[abc]/\n\0/' $temp_file
        sed -i 's/[def]/\n\0/' $temp_file
        sed -i 's/[ghi]/\n\0/' $temp_file
        sed -i 's/[jkl]/ \0/' $temp_file
    else
        sed -i 's/[zxy]/\n\0/' $temp_file
        sed -i 's/[wvu]/\n\0/' $temp_file
        sed -i 's/[tsr]/\n\0/' $temp_file
        sed -i 's/[qpo]/ \0/' $temp_file
    fi
    if [ $random_number -eq 0 ]; then
        shuf -n 8 $temp_file > $out_file
    else
        grep -v `expr substr "$content" 1 2` $temp_file > $out_file
    fi
    rm $temp_file

    # generate commit
    git add $output_directory
    if [ -n "$changed" ]; then
        message_head="Change "
    else
        message_head="Add "
    fi
    get_branch_name
    if [ -n "$branch" ]; then
        message_tail=" on branch $branch"
    fi
    git commit -m "$message_head$file_name$message_tail"
}

get_branch_name() {
    branch=`git branch | grep \* | cut -d' ' -f2`
}



# ---  D E F A U L T   S E T T I N G S  --- #
number_of_commits="oo"
output_directory="generated_files"



# ---  M A I N   S C R I P T  --- #
while getopts ":b:f:n:o:p:" opt; do
    case $opt in
        b)
            target_branch=$OPTARG
            ;;

        f)
            output_file=$OPTARG
            ;;

        n)
            number_of_commits=$OPTARG
            ;;

        o)
            output_directory=$OPTARG
            ;;

        p)
            parent_branch=$OPTARG
            ;;

        ?|:)
            print_help_message
            exit
    esac
done

git rev-parse || {
    echo "You must execute this script in the working directory of a git repository."
    exit
}

if [ -n "$target_branch" ]; then
    # if necessary, generate specified branch
    if [ -z "`git branch | grep $target_branch`" ]; then
        git branch $target_branch $parent_branch
    fi

    # checkout target branch
    get_branch_name
    old_branch=$branch
    git checkout $target_branch
fi

if [ $number_of_commits = "oo" ]; then
    # generate commits until receiving SIGINT
    while [ "true" ]; do
        generate_commit
    done
else
    # generate specified number of commits
    for i in `seq $number_of_commits`; do
        generate_commit
    done
fi

if [ -n "$target_branch" ]; then
    # checkout previous branch
    git checkout $old_branch
fi
