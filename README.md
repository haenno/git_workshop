# Git - Workshop

This repository is merely a playground for a Git workshop. Its main purpose is to provide scripts
that quickly and easily generate repositories with random commits for testing purposes.


## Getting started
To be able to use the scripts, add them to your `$PATH`:
```
git clone git@github.com:haggl/git_workshop.git
source git_workshop/configure.sh
```

You should now be able to run the generators using git:
```
git workshop generate-commits
```

To gain more information on the scripts, use their `-h` option to display usage hints.

## Exercises
The most interesting script is probably `git workshop setup-exercise`.
It can be used to setup example repositories for things that can be kind of hard to wrap your `HEAD` around:
* rebasing / merging branches
* merge conflicts /o\
* working with submodules

//2022 Testcommit// 
